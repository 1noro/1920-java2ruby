#!/usr/bin/env ruby
require_relative "Alta"

# --- VARIABLES GLOBALES -------------------------------------------------------
$finAltas = "fin";
$overwriteFile = 'a';

nMaxOpt = 4;
dir = "/opt/archivosJava/";
filename = "altasEdadRb01.dat";

# --- INTERFAZ DE USUARIO ------------------------------------------------------
def printMenu()
    puts ""
	puts "--- MENU ---"
	puts " 1 - Altas"
	puts " 2 - Listados"
	puts " 3 - Media de edad"
	puts " 4 - Nombre del mayor y menor (no rep.)"
	puts ""
	puts " 0 - Salir"
	puts ""
end

def readOpt(max)
    out = 0
    loop do
        print "Escribe una opción: "
        out = gets.to_i
        break if (out >= 0 && out <= max)
    end
    return out
end

# --- LECTURAS Y VALIDACIONES DE FORMATO ---------------------------------------
def readNombre()
    nombre = ""
    loop do
        print " Nombre (15 char max); '" + $finAltas + "' para acabar: "
        nombre = gets.chomp #lee sin el \n del final
        break if (nombre == $finAltas || nombre.length < 15)
    end
    return nombre
end

def readEdad()
    edad = ""
    continuar = true
    loop do
        print " Edad: "
        edad = gets.chomp #lee sin el \n del final
        begin
            edad = Integer(edad)
            continuar = false
        rescue
        end
        break if !continuar
    end
    return edad
end

# --- FUNCIONES GENÉRICAS ------------------------------------------------------
def saveAlta(linea, fullDir)
    puts " \#GUARADO: " + linea
    begin
        File.write(fullDir, linea + "\n", mode: $overwriteFile)
    rescue
        puts "[FAIL] Algo falló."
    end
end

def altas(fullDir)
    myAlta = nil
    nombre = ""
    edad = 0

    loop do
        puts "\n>> Nueva alta:"
        nombre = readNombre()
        if (nombre != $finAltas)
            edad = readEdad()
            myAlta = Alta.new(nombre, edad)
            saveAlta(myAlta.toStringForFile, fullDir)
        end
        break if (nombre == $finAltas)
    end
end

def printListados(fullDir)
    begin
        File.readlines(fullDir).each do |line|
            puts Alta.newFromLine(line).toString
        end
    rescue
        puts "[FAIL] Algo falló."
    end
end

def printMediaEdad(fullDir)
    cont = 0
    suma = 0
    begin
        File.readlines(fullDir).each do |line|
            suma += Alta.newFromLine(line).getEdad
            cont += 1
        end
        puts "Media: " + suma.to_s + " / " + cont.to_s + " = " + (suma / cont).to_s
    rescue
        puts "[FAIL] Algo falló."
    end
end

def printNombreMayorMenor(fullDir)
    eMax = 0
    eMin = 999
    nMax = ""
    nMin = ""
    begin
        File.readlines(fullDir).each do |line|
            myAlta = Alta.newFromLine(line)
            if (myAlta.getEdad > eMax)
                eMax = myAlta.getEdad
                nMax = myAlta.getNombre
            end
            if (myAlta.getEdad < eMin)
                eMin = myAlta.getEdad
                nMin = myAlta.getNombre
            end
        end
        puts "Mayor: " + nMax + " (" + eMax.to_s + ")"
        puts "Menor: " + nMin + " (" + eMin.to_s + ")"
    rescue
        puts "[FAIL] Algo falló."
    end
end

# --- MAIN ---------------------------------------------------------------------
opt = 0

loop do
    printMenu()
    opt = readOpt(nMaxOpt)
    case opt
        when 1
            altas(dir + filename)
        when 2
            printListados(dir + filename)
        when 3
            printMediaEdad(dir + filename)
        when 4
            printNombreMayorMenor(dir + filename)
    end
    break if opt == 0
end
