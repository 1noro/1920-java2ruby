
class Alta

    def initialize(_nombre, _edad)
        @nombre = _nombre
        @edad = Integer(_edad)
    end

    def self.newFromLine(line) #self.nombreMetodo == public static
        part = line.gsub(/\n/, '').split('::')
        return self.new(part[0], part[1])
    end

    def getNombre() return @nombre end
    def getEdad() return @edad end
    def setNombre(_nombre) @nombre = _nombre end
    def setEdad(_edad) @edad = _edad end

    def toStringForFile() return @nombre + "::" + @edad.to_s end
    def toString() return @nombre + "\t\t" + @edad.to_s end

end
